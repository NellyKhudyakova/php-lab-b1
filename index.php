<!DOCTYPE html>
<html lang="en">
<head>
<!--    <meta charset="windows-1251">-->
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №B-1</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №B-1</h1></div>
    <div></div>
</header>

<main>
    <?php

    require 'menu.php';

    if( $_GET['p'] == 'viewer' ) {
        include 'viewer.php';

        if( !isset($_GET['pg']) || $_GET['pg']<0 ) $_GET['pg']=0;

        if(!isset($_GET['sort']) || ($_GET['sort']!='byid' && $_GET['sort']!='fam' && $_GET['sort']!='birth' && $_GET['sort']!='upd')){
            $_GET['sort']='byid';
        }

        echo getContactBook($_GET['sort'], $_GET['pg']);
    }
    else if( $_GET['p'] == 'add' ) { include 'add.php'; }
    else if( $_GET['p'] == 'edit' ) { include 'edit.php'; }
    else if( $_GET['p'] == 'delete' ) { include 'testDel.php'; }

    ?>
</main>

<footer>

</footer>

</body>
</html>
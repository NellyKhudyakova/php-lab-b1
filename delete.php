<?php

$mysqli = mysqli_connect('localhost', 'root', '', 'contacts');
mysqli_set_charset($mysqli, "utf8");

if (mysqli_connect_errno()) {
    echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
}

$sql_res = mysqli_query($mysqli, 'SELECT * FROM people');

if (!mysqli_errno($mysqli)) {
    echo '<div id="edit_links">';
    echo '<h3>Контакты, доступные для удаления</h3>';

    $currentROW = array();

    while ($row = mysqli_fetch_assoc($sql_res)) {
        if (!$currentROW && (!isset($_GET['id']) || $_GET['id'] == $row['id'])) {
            $currentROW = $row;
            echo '<div class="editable">' . $row['last_name'] . ' ' . substr($row['first_name'], 0, 2) . ' ' . substr($row['middle_name'], 0, 2) . '</div>';
        } else {
            echo '<a href="?p=delete&id=' . $row['id'] . '&last_name=' . $row['last_name'] . '" class="for_edit">' . $row['last_name'] . ' ' . substr($row['first_name'], 0, 2) . ' ' . substr($row['middle_name'], 0, 2) . '</a>';
        }
    }
    echo '</div>';

    echo '<form name="form_delete" method="post" action="index.php?p=delete';
    if ($currentROW) {
        echo '&last_name=' . $_GET['last_name'] . '&id=' . $_GET['id'];
    }
    echo '">';

    echo '<input type="submit" name="button" value="Удалить запись">
    <input type="hidden" name="id" value="';
    if ($currentROW){
        echo $currentROW['id'];
    }
    echo '"></form>';
}

if (isset($_POST['button']) && $_POST['button'] == 'Удалить запись') {

    @$sql_res = mysqli_query($mysqli, 'DELETE FROM people WHERE id =' . $_GET['id'] );
//    $url = 'index.php?p=delete&last_name=' . $_GET['last_name'] . '&id=' . $_GET['id'];
//    header("Refresh:0; $url");

    if (mysqli_errno($mysqli)) {
        echo '<div class="error">Запись не удалена</div>';
    } else {
        echo '<div class="ok">Запись с фамилией ' . $_GET['last_name'] .' удалена</div>';
    }
}

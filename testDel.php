<?php
$mysqli = mysqli_connect('localhost', 'root', '', 'contacts');
mysqli_set_charset($mysqli, "utf8");

if (mysqli_connect_errno()) {
    echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
} else {

    if (isset($_POST['button']) && $_POST['button'] == 'Удалить запись') {

        @$sql_res = mysqli_query($mysqli, 'DELETE FROM people WHERE id =' . explode(' ', $_POST['person'])[0]);

        if (mysqli_errno($mysqli)) {
            echo '<div class="error">Запись не удалена</div>';
        } else {
            echo '<div class="ok">Запись с фамилией ' . explode(' ', $_POST['person'])[1] . ' удалена</div>';
        }
    }

    $sql_res = mysqli_query($mysqli, 'SELECT * FROM people');

    if (!mysqli_errno($mysqli)) {
        echo '<div id="del_links">';
        echo '<h3>Контакты, доступные для удаления</h3>';
        echo '<form name="form_delete" method="post" action="index.php?p=delete">';

        while ($row = mysqli_fetch_assoc($sql_res)) {
            echo '<div class="choiceBlock">';
            echo '<input type="radio" id="personChoice' . $row['id'] . '" name="person" value="' . $row['id'] . ' ' . $row['last_name'] . '">';

            echo '<label for="personChoice' . $row['id'] . '">' . $row['last_name'] . ' ' .
                substr($row['first_name'], 0, 2) . ' ' . substr($row['middle_name'], 0, 2) . '</label>';
            echo '</div>';
        }

        echo '<input type="submit" name="button" value="Удалить запись">';

        echo '</form>';
        echo '</div>';
    }

}
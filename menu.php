<div id="menu">
    <?php

    // если нет параметра меню – добавляем его
    if (!isset($_GET['p'])){
        $_GET['p'] = 'view';
    }

    echo '<a href="index.php?p=viewer"'; // 1 пункт меню
    if ($_GET['p'] == 'viewer'){ // если он выбран
        echo ' class="selected"'; // выделяем его
    }
    echo '>Просмотр</a>';

    echo '<a href="index.php?p=add"'; // 2 пункт меню
    if ($_GET['p'] == 'add'){
        echo ' class="selected"';
    }
    echo '>Добавление записи</a>';

    echo '<a href="index.php?p=edit"'; // 3 пункт меню
    if ($_GET['p'] == 'edit'){
        echo ' class="selected"';
    }
    echo '>Рерактирование записи</a>';

    echo '<a href="index.php?p=delete"'; // 4 пункт меню
    if ($_GET['p'] == 'delete'){
        echo ' class="selected"';
    }
    echo '>Удаление записи</a>';

    if ($_GET['p'] == 'viewer') //если был выбран первый пунт меню
    {
        echo '<div id="submenu">'; // выводим подменю
        echo '<a href="index.php?p=viewer&sort=byid "';// 1 пункт подменю

        if (!isset($_GET['sort']) || $_GET['sort'] == 'byid'){
            echo ' class="selected"';
        }
        echo '>По-умолчанию</a>';

        echo '<a href="index.php?p=viewer&sort=fam"'; // 2 пункт подменю
        if (isset($_GET['sort']) && $_GET['sort'] == 'fam'){
            echo ' class="selected"';
        }
        echo '>По фамилии</a>';

        echo '<a href="index.php?p=viewer&sort=upd"'; // 3 пункт подменю
        if (isset($_GET['sort']) && $_GET['sort'] == 'upd'){
            echo ' class="selected"';
        }
        echo '>По дате обновления</a>';

        echo '<a href="index.php?p=viewer&sort=birth"'; // 4 пункт подменю
        if (isset($_GET['sort']) && $_GET['sort'] == 'birth'){
            echo ' class="selected"';
        }
        echo '>По дате рождения</a>';
        echo '</div>'; // конец подменю
    }

    //   проверка параметра p на корректность значения

    ?>
</div>

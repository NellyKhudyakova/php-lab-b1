<?php
//echo '<h3>viewer</h3>';
//            print_r($sql_res);
//            print_r(mysqli_fetch_assoc($sql_res));

    function getContactBook($type, $page)
    {
        $mysqli = @mysqli_connect('localhost', 'root', '', 'contacts');

        if (!$mysqli){
            return 'Ошибка подключения к БД. Код: ' . mysqli_connect_errno();
        }

        mysqli_set_charset($mysqli, "utf8");

        $sql_res = mysqli_query($mysqli, 'SELECT COUNT(*) FROM people');

        if (!mysqli_errno($mysqli) && $row = mysqli_fetch_row($sql_res))
        {
            if (!$TOTAL = $row[0])
                return 'В таблице нет данных';
            $PAGES = ceil($TOTAL / 10);
            if ($page >= $TOTAL)
                $page = $TOTAL - 1;


            switch ($type) {
                case 'byid':
                    $sql = 'SELECT * FROM people ORDER BY id LIMIT ' . ($page * 10) . ',10';
                    break;
                case 'fam':
                    $sql = 'SELECT * FROM people ORDER BY last_name LIMIT ' . ($page * 10) . ',10';
                    break;
                case 'upd':
                    $sql = 'SELECT * FROM people ORDER BY modification_date DESC LIMIT ' . ($page * 10) . ',10';
                    break;
                case 'birth':
                    $sql = 'SELECT * FROM people ORDER BY birth_date  LIMIT ' . ($page * 10) . ',10';
                    break;
            }


            $sql_res = mysqli_query($mysqli, $sql);

            $ret = '<table>';
            $ret .='<tr><td>Фамилия</td>
                        <td>Имя</td>
                        <td>Отчество</td>
                        <td>Пол</td>
                        <td>Дата рождения</td>
                        <td>Телефон</td>
                        <td>Адрес</td>
                        <td>E-mail</td>
                        <td>Комментарий</td></tr>';

            while ($row = mysqli_fetch_assoc($sql_res)) {

                $ret .= '<tr><td>' . $row['last_name'] . '</td>
                             <td>' . $row['first_name'] . '</td>
                             <td>' . $row['middle_name'] . '</td>
                             <td>' . $row['gender'] . '</td>
                             <td>' . $row['birth_date'] . '</td>
                             <td>' . $row['phone'] . '</td>
                             <td>' . $row['address'] . '</td>
                             <td>' . $row['email'] . '</td>
                             <td>' . $row['comment'] . '</td></tr>';
            }
            $ret .= '</table>';
            if ($PAGES > 1) {
                $ret .= '<div id="pagination">';
                for ($i = 0; $i < $PAGES; $i++)
                    if ($i != $page)
                        $ret .= '<a href="?p=viewer&sort=' . $type . '&pg=' . $i . '">' . ($i + 1) . '</a>';
                    else
                        $ret .= '<span>' . ($i + 1) . '</span>';
                $ret .= '</div>';
            }
            return $ret;
        }

        return 'Неизвестная ошибка';
    }

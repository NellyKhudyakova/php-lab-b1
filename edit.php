<?php

$mysqli = mysqli_connect('localhost', 'root', '', 'contacts');
mysqli_set_charset($mysqli, "utf8");

if (mysqli_connect_errno()) {
    echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
    exit();
}

if (isset($_POST['button']) && $_POST['button'] == 'Изменить запись')
{
    $sql_res = mysqli_query($mysqli, 'UPDATE people SET last_name="' .
        htmlspecialchars($_POST['last_name']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET first_name="' .
        htmlspecialchars($_POST['first_name']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET middle_name="' .
        htmlspecialchars($_POST['middle_name']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET gender="' .
        htmlspecialchars($_POST['gender']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET birth_date="' .
        htmlspecialchars($_POST['birth_date']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET phone="' .
        htmlspecialchars($_POST['phone']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET address="' .
        htmlspecialchars($_POST['address']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET email="' .
        htmlspecialchars($_POST['email']) . '"WHERE id=' . $_POST['id']);

    $sql_res = mysqli_query($mysqli, 'UPDATE people SET comment="' .
        htmlspecialchars($_POST['comment']) . '"WHERE id=' . $_POST['id']);

    echo '<div class="ok">Данные изменены</div>';

    $_GET['id'] = $_POST['id'];
}

$sql_res = mysqli_query($mysqli, 'SELECT * FROM people');

if (!mysqli_errno($mysqli))
{
    $currentROW = array();

    echo '<div id="edit_links">';
    echo '<h3>Контакты, доступные для изменения</h3>';

    while ($row = mysqli_fetch_assoc($sql_res)) {
        if (!$currentROW && (!isset($_GET['id']) || $_GET['id'] == $row['id']))
        {
            $currentROW = $row;
            echo '<div class="editable">' . $row['last_name'] . ' ' . $row['first_name'] . '</div>';
        } else {
            echo '<a href="?p=edit&id=' . $row['id'] . '" class="for_edit">' . $row['last_name'] . ' ' . $row['first_name'] . '</a>';
        }
    }
    echo '</div>';

    echo '<form name="form_edit" method="post" action="index.php?p=edit">';

    echo '<input type="text" name="last_name" id="last_name" ';
        if ($currentROW) echo ' value="' . $currentROW['last_name'] . '"';
    echo '>';

    echo '<input type="text" name="first_name" id="first_name" placeholder="Имя" ';
        if ($currentROW) echo ' value="' . $currentROW['first_name'] . '"';
    echo '>';

    echo '<input type="text" name="middle_name" id="middle_name" placeholder="Отчество" ';
        if ($currentROW) echo ' value="' . $currentROW['middle_name'] . '"';
    echo '>';

    echo '<input type="text" name="gender" id="gender" placeholder="Пол" ';
        if ($currentROW) echo ' value="' . $currentROW['gender'] . '"';
    echo '>';

    echo '<input type="date" name="birth_date" id="birth_date" placeholder="Дата рождения" ';
        if ($currentROW) echo ' value="' . $currentROW['birth_date'] . '"';
    echo '>';

    echo '<input type="tel" name="phone" id="phone" placeholder="Телефон" ';
        if ($currentROW) echo ' value="' . $currentROW['phone'] . '"';
    echo '>';

    echo '<textarea name="address" placeholder="Адрес">';
        if ($currentROW) echo $currentROW['address'];
    echo '</textarea>';

    echo '<input type="email" name="email" id="email" placeholder="E-mail" ';
    if ($currentROW) echo ' value="' . $currentROW['email'] . '"';
    echo '>';

    echo '<textarea name="comment" placeholder="Краткий комментарий" >';
    if ($currentROW) echo $currentROW['comment'];
    echo '</textarea>';

    echo '<input type="submit" name="button" value="Изменить запись">
    <input type="hidden" name="id" value="';
        if ($currentROW){
            echo $currentROW['id'];
        }
    echo '"></form>';

} else echo '<div class="error">Ошибка базы данных</div>';


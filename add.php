<form name="form_add" method="post" action="index.php?p=add">

    <input type="text" name="last_name" id="last_name" placeholder="Фамилия">

    <input type="text" name="first_name" id="first_name" placeholder="Имя">

    <input type="text" name="middle_name" id="middle_name" placeholder="Отчество">

    <input type="text" name="gender" id="gender" placeholder="Пол">

    <input type="date" name="birth_date" id="birth_date" placeholder="Дата рождения">

    <input type="tel" name="phone" id="phone" placeholder="Телефон">

    <textarea name="address" placeholder="Адрес"></textarea>

    <input type="email" name="email" id="email" placeholder="E-mail">

    <textarea name="comment" placeholder="Краткий комментарий"></textarea>

    <input type="submit" name="button" value="Добавить запись">

</form>

<?php

if (isset($_POST['button']) && $_POST['button']== 'Добавить запись')
{
    $mysqli = mysqli_connect('localhost', 'root', '', 'contacts');
    mysqli_set_charset($mysqli, "utf8");

    if (mysqli_connect_errno()){
        echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
    }
    date_default_timezone_set('Europe/Moscow');
    $curTime = date("Y-m-d H:i:s");
    $sql_res = mysqli_query($mysqli, 'INSERT INTO people VALUES ("", "' .
        htmlspecialchars($_POST['last_name']) . '", "' .
        htmlspecialchars($_POST['first_name']) . '", "' .
        htmlspecialchars($_POST['middle_name']) . '", "' .
        htmlspecialchars($_POST['gender']) . '", "' .
        htmlspecialchars($_POST['birth_date']) . '", "' .
        htmlspecialchars($_POST['phone']) . '", "' .
        htmlspecialchars($_POST['address']) . '", "' .
        htmlspecialchars($_POST['email']) . '", "' .
        htmlspecialchars($_POST['comment']) . '", "'.$curTime.'")');


    if (mysqli_errno($mysqli)){
        echo '<div class="error">Запись не добавлена ' . mysqli_error($mysqli) .'</div>';

    } else
        echo '<div class="ok">Запись добавлена</div>';
}
?>